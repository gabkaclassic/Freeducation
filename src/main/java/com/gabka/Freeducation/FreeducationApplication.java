package com.gabka.Freeducation;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import static org.springframework.boot.SpringApplication.*;

@SpringBootApplication
public class FreeducationApplication {

	public static void main(String[] args) {
		
		run(FreeducationApplication.class, args);
	}

}
